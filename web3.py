from flask import Flask, session
from flask_restful import Api, Resource, abort, reqparse
app = Flask(__name__)
app.secret_key = "secret key"
api = Api(app)

argument = reqparse.RequestParser()
argument.add_argument("name", type=str, help="name is missing", required=True)
argument.add_argument("movie_director", type=str, help="movie director is missing", required=True)
argument.add_argument("year", type=int, help="Published year is missing", required=True)
argument.add_argument("avaiable_platform", type=str, help="avaiable platform is missing", required=True)

movies = {}

class movie(Resource):
    def get(self, movie_name):
        flag = 0
        count = 0
        for i in range(len(movies)):
            if movies[i]["name"] == movie_name:
                flag = 1
                break
            else:
                count+=1

        if flag == 0:
            abort(404, message="There is no movie")
        return movies[count]

    def put(self, movie_name):
        arguments = argument.parse_args()
        count = len(movies)
        for i in range(len(movies)):
            if movies[i]["name"] == movie_name:
                abort(409, message="Already exists")
        movies[count] = arguments
        return movies[count], 201

    def delete(self, movie_name):
        flag = 0
        count = 0
        for rows in range(len(movies)):
            if rows["name"] == movie_name:
                flag = 1
                break
            else:
                count+=1
        if flag == 0:
            abort(404, message="Data {} doesn't exist".format(movie_name))
        del movies[count]
        return '', 204

api.add_resource(movie, "/movie/<movie_name>")
if __name__ == '__main__':
    app.run(host = '0.0.0.0', debug=True)
