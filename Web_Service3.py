from flask import Flask, session
from flask_restful import Api, Resource, abort, reqparse, fields, marshal_with
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.secret_key = "secret key"
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
db = SQLAlchemy(app)
api = Api(app)

class MovieDB(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    year = db.Column(db.String(50), nullable=False)
    director = db.Column(db.String(20), nullable=False)
    avaiable_platform = db.Column(db.String(100), nullable=False)

    def __init__(self, id, name, year, director, avaiable_platform):
        self.id = id
        self.name = name
        self.year = year
        self.director = director
        self.avaiable_platform = avaiable_platform

argument = reqparse.RequestParser()
argument.add_argument("movie_director", type=str, help="movie director is missing", required=True)
argument.add_argument("year", type=int, help="Published year is missing", required=True)
argument.add_argument("avaiable_platform", type=str, help="avaiable platform is missing", required=True)

movies = {'id': fields.Integer,'name': fields.String,'year': fields.Integer,'director': fields.String,'avaiable_platform': fields.String }

class movie(Resource):
    @marshal_with(movies)
    def get(self, movie_name):
        result = MovieDB.query.filter_by(name=movie_name).all()
        if not result:
		          abort(404, message="There is no movie.")
        return result

    @marshal_with(movies)
    def put(self, movie_name):
        arguments = argument.parse_args()
        result = MovieDB.query.filter_by(name=movie_name).first()
        if result:
            abort(409, message="Movie already exists")
        count = MovieDB.query.count()

        id = 0
        if count == 0:
            movie = MovieDB(id=1,name=movie_name,year=arguments["year"],director=arguments["movie_director"], avaiable_platform=arguments["avaiable_platform"])
            db.session.add(movie)
            db.session.commit()
            return movie, 201

        for i in range(count):
            movie =  MovieDB.query.filter_by(id=i+1).first()
            if not movie:
                id = i + 1
                break

        movie = MovieDB(id=id,name=movie_name,year=arguments["year"],director=arguments["movie_director"], avaiable_platform=arguments["avaiable_platform"])
        db.session.add(movie)
        db.session.commit()

        return movie, 201

    def delete(self, movie_name):
        result = MovieDB.query.filter_by(name=movie_name).first()
        if not result:
            abort(404, message="Data {} doesn't exist".format(movie_name))
        MovieDB.query.filter_by(name=movie_name).delete()
        db.session.commit()
        return '', 204

api.add_resource(movie, "/movie/<movie_name>")

if __name__ == '__main__':
    app.run(host = '0.0.0.0', debug=True)
